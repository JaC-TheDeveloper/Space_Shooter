﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByContact : MonoBehaviour {
	public GameObject explosion;
	private Transform transform;
	public GameObject playerExplosion;
	public GameController gameController;
	public int scoreValue;
	void Start(){
		GameObject obj = GameObject.FindWithTag("GameController");
		if(obj != null){
			gameController = obj.GetComponent<GameController>();
		}
		else
			Debug.Log("Cannot find Game Controller");
		transform = GetComponent<Transform>();
	}
	void OnTriggerEnter(Collider other){
		if(other.tag == "Boundary")
			return;
		Instantiate (explosion, transform.position, transform.rotation) ;
        if (other.tag == "Player")
        {
            Instantiate(playerExplosion, other.transform.position, other.transform.rotation);
            gameController.gameOver();
        }
        else
            gameController.AddScore(scoreValue);
		Destroy(other.gameObject);
		Destroy(gameObject);
		
	}
}
