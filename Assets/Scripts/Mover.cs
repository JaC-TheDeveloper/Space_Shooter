﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour {
	private Rigidbody rb;
	private Transform transform;
	public float speed;
	void Start(){
		rb = GetComponent<Rigidbody>();
		transform = GetComponent<Transform>();
		rb.velocity = transform.forward*speed;
        
	}
}
