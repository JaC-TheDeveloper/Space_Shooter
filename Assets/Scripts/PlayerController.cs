﻿using UnityEngine;
using System.Collections;
[System.Serializable]
public class Boundary{
	public float xMin,xMax,zMin,zMax;
}

public class PlayerController : MonoBehaviour
{
	public float speed;
	public Boundary boundary;
	private Rigidbody rb;
	public float tilt;
	private float nextFire = 0.0f;
	public float fireRate = 0.5f;
	public GameObject shot;
	public Transform shotSpawn;
	private AudioSource audio;
	void Start(){
		rb = GetComponent<Rigidbody>();
		audio = GetComponent<AudioSource>();
		speed = 10.0f;
	}

	void Update(){
		if(Input.GetButton("Fire1") && Time.time > nextFire){
			nextFire = Time.time + fireRate;
			//GameObject clone = 
			Instantiate (shot, shotSpawn.position, shotSpawn.rotation) ;
			//as GameObject;
			audio.Play();
		}
	}
	void FixedUpdate ()
	{
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");

		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
		rb.velocity = movement * speed;
		rb.position = new Vector3(Mathf.Clamp( rb.position.x,boundary.xMin,boundary.xMax),
								0.0f,
								Mathf.Clamp(rb.position.z,boundary.zMin,boundary.zMax));
		
		
		rb.rotation = Quaternion.Euler(0.0f,0.0f,rb.velocity.x * -tilt);
		} //-12 - 12 //14 - -1
}
